<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ApiProvider extends ServiceProvider
{

    public static function makeRequest($url = "", $body = array(), $header = "", $method = "", $body_is_array = true){

        $curl = curl_init();

        if($body_is_array){
            if(!empty($body)){

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 300,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => $method,
                    CURLOPT_POST => ($method == "POST"),
                    CURLOPT_POSTFIELDS => json_encode($body),
                    CURLOPT_HTTPHEADER => ($header),
                ));
            }
        }elseif(!$body_is_array){
            if($header == ""){
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 300,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => $method,
                    CURLOPT_POST => ($method == "POST"),
                    CURLOPT_POSTFIELDS => ($body),
                ));
            }else{
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 300,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => $method,
                    CURLOPT_POST => ($method == "POST"),
                    CURLOPT_POSTFIELDS => ($body),
                    CURLOPT_HTTPHEADER => $header,
                ));
            }
        }else{
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 300,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_POST => ($method == "POST"),
                CURLOPT_HTTPHEADER => $header,
            ));
        }

        $response = json_decode(curl_exec($curl));
        
        $code =  curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $return = array(
            'code'        => $code,
            'requisition' => $response
        );

        curl_close($curl);

        return (object)$return;



    }

}
