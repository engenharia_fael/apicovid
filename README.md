##### EXPLICAÇÃO

    No controller ApiController, função CovidSearch(), tem a requisição CURL gerada pelo POSTMAN, após o consumo da API diretamente com a 
    url: https://api.brasil.io/v1/dataset/covid19/caso/data/?state=PR&date=2020-05-12

    Porém, transportar para o PHP, o mesmo não funciona. Exibe mensagem dizendo que o limite de requisições foi atingido. Para continuar o teste, eu peguei 3 datas:

    1) 10/05/2020
    2) 11/05/2020
    3) 12/05/2020

    Armazenei na função list() também em ApiController. Segui como se a API tivesse retornado o resultado. A URL de testes: http://localhost:8000/?state=PR&dateStart=2020-05-10&dateEnd=2020-05-12

##### Documentação API

    - EndPoint: http://localhost:8000/
    - Method:   GET
    - Params:  
        - state (required): Estado a ser pesquisado
        - dateStart (required): data do ínicio do período a ser pesquisado
        - dateEnd (required): data do final do período a ser pesquisado
    - Exemplo: http://localhost:8000/?state=PR&dateStart=2020-05-10&dateEnd=2020-05-12
 